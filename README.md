## Installation

A repository containing commit hooks that are run inside a docker container to keep your local machine clean of any dependencies. Installer can be run on both Linux and macOS. Simply run ```setup.sh``` and the commit hooks will be installed for you. For existing local repositories you may want to run ```git init``` for the global commit hooks to become active in that repository.

## Supported Linters

| Linter        | Syntax       | File extension |
| --------------|--------------|----------------|
| Bandit        | Python       | .py            |
| Flak8         | Python       | .py            |
| Shellcheck    | Shell / Bash | .sh            |
| Yamllint      | Yaml         | .yaml / .yml   |
| Jsonlint      | Json         | .json          |
| Eslint        | Javascript   | .js            |
| Golint        | Golang       | .go            |
| Tflint        | Terraform    | .tf / .tfvars  |


Note: the Bash spinner included in this is project is copied from another [project](https://github.com/DevelopersToolbox/bash-spinnerµ˜).

## Testing

To test changes to the commit hooks script, you first have to build a new docker container from the root of this repository.

```
docker build --tag commit-hooks:latest ./docker
```

Afterwards you should add some test files with ```git add```. If this is done correctly, you can run the docker container.

```
repo_root=$(git rev-parse --show-toplevel)
docker run -v ${repo_root}:/workspace/repo --rm commit-hooks:latest /workspace/lint.sh
```
