#!/bin/bash
#
# Installing global commit hooks and building commit-hooks docker container
#

echo "Installing commit hooks..."

echo "Verifying docker installation..."

if docker -v > /dev/null 2>&1; then
    echo -e " + Docker is installed"
else
    echo -e " - Docker is not installed, please install docker first..."
    exit 1
fi

if docker info > /dev/null 2>&1; then
    echo -e " + Docker is running"
else
    echo -e " - Docker is not running, please start docker first ..."
    exit 1
fi

basedir="${PWD}"

echo "Setting global core.hooksPath..."

if git config --global core.hooksPath "${basedir}"/hooks > /dev/null 2>&1; then
    echo -e " + core.hooksPath is set to ${basedir}/hooks"
else
    echo -e " - An error occured while setting git core.hooksPath"
    exit 1
fi

echo "Installing commit-hooks docker container..."

docker build --tag commit-hooks:latest "$basedir/docker"
