FROM python:3.9-slim
WORKDIR /workspace

RUN pip install bandit flake8 yamllint

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y apt-utils golang curl git nodejs shellcheck unzip

RUN curl -sL https://www.npmjs.com/install.sh | bash -

RUN npm install -g typescript eslint

ENV GOPATH $HOME/go
ENV PATH $PATH:$GOPATH/bin
ENV GOBIN $PATH:$GOPATH/bin

RUN go get -u golang.org/x/lint/golint

RUN curl -L "$(curl -Ls https://api.github.com/repos/terraform-linters/tflint/releases/latest \
    | grep -o -E "https://.+?_linux_amd64.zip")" -o tflint.zip \
    && unzip tflint.zip \
    && install tflint /usr/local/bin \
    && rm tflint.zip

RUN mkdir /workspace/repo

COPY lint.sh /workspace/lint.sh
COPY spinner.sh /workspace/spinner.sh
